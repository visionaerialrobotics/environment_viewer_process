/*!*******************************************************************************************
 *  \file       mission_state_receiver.h
 *  \brief      MissionStateReceiver definition file.
 *  \details    This class is in charge of receiving the actions performed in the
 *              Cooperative Interaction and Guided by Mission Planner modes
 *              (approved and completed actions). It also asks for the mission to start/stop
 *              or its name, and receives mission-related information such as completion status
 *              and task performed.
 *  \author     Yolanda de la Hoz Simon, German Quintero
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef HumanMachineInterface_MISSIONSTATERECEIVER_H_
#define HumanMachineInterface_MISSIONSTATERECEIVER_H_

#include <ros/ros.h>
#include <string>
#include <pugixml.hpp>
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"
#include "droneMsgsROS/droneMissionInfo.h"
#include <QString>
#include <QtDebug>
#include <QStringListModel>
#include "droneMsgsROS/actionData.h"
#include "droneMsgsROS/actionArguments.h"
#include "droneMsgsROS/openMissionFile.h"
#include "droneMsgsROS/missionName.h"
#include "droneMsgsROS/CompletedAction.h"
#include "droneMsgsROS/PublicEvent.h"
#include "droneMsgsROS/Event.h"
#include "droneMsgsROS/CompletedMission.h"

class MissionStateReceiver : public QObject
{
  Q_OBJECT
public:
  // Constructor & Destructor
  MissionStateReceiver();
  ~MissionStateReceiver();

public:
  /*!************************************************************************
   *  \brief  This method returns whether the receiver is ready or not.
   *  \return True if the communication with the ROS node has been established; false otherwise.
   *************************************************************************/
  bool ready();

  /*!************************************************************************
   *  \brief  This method establishes the appropiate connections to the ROS topics and services needed.
   *  \param nodeHandle The HMI ROS node.
   *  \param rosnamespace The HMI ROS namespace.
   *************************************************************************/
  void openSubscriptions(ros::NodeHandle nodeHandle, std::string rosnamespace);

  /*!************************************************************************
   *  \brief  This method sets the autonomous mode indicator to active.
   *************************************************************************/
  void activateAutonomousMode();

  /*!************************************************************************
   *  \brief  This method sets the cooperative mode indicator to inactive.
   *  \details Requests mission name.
   *************************************************************************/
  void deactivateAutonomousMode();

  /*!************************************************************************
   *  \brief  This method gets the arguments of the current action
   *************************************************************************/
  QString getActionArguments();

  /*!************************************************************************
   *  \brief  This method calls the start mission service.
   *  \return True if the service was called; false otherwise
   *************************************************************************/
  bool startMission();

  /*!************************************************************************
   *  \brief  This method calls the start mission service.
   *  \return True if the service was called; false otherwise
   *************************************************************************/
  bool abortMission();

  /*!************************************************************************
   *  \brief  This method calls the mission name service.
   *  \return True if the service was called; false otherwise
   *************************************************************************/
  bool requestMissionName();

  /*!************************************************************************
   *  \brief  This method emits the emergency land signal.
   *************************************************************************/
  void emitEmergencyLand();

  /*!************************************************************************
   *  \brief  This method loads a mission via a file path.
   *************************************************************************/
  bool loadMission(const std::string file_path);

  // Indicates if the guided by mission planner mode is active or not.
  bool is_autonomous_mode_active;

private:
  std::vector<std::string> active_drones;

  // ROS Topics
  std::string current_task_topic;
  std::string current_action_topic;
  std::string completed_mission_topic;
  std::string completed_action_topic;
  std::string public_event_topic;
  std::string event_topic;
  std::string my_stack_directory;
  std::string rosnamespace;

  // Current action
  droneMsgsROS::actionData current_action;
  // Curent task name
  std::string task_name;

  // Indicates if the communication with the ROS node has been established or not.
  bool subscriptions_complete;

  // Current action arguments
  QString arguments;

  // ROS Service clients
  ros::ServiceClient load_mission_client; /** UNUSED */
  ros::ServiceClient mission_name_client;
  ros::ServiceClient start_mission_client;
  ros::ServiceClient stop_mission_client;

  // ROS subscribers
  ros::Subscriber current_task_subs;
  ros::Subscriber current_action_subs;
  ros::Subscriber completed_mission_subs;
  ros::Subscriber completed_action_subs;
  ros::Subscriber public_event_sub;
  ros::Subscriber event_sub;

  /*!************************************************************************
   *  \brief   Processes the current action being executed to display it correctly.
   ***************************************************************************/
  void currentActionCallback(const droneMsgsROS::actionData::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the current task being executed to display it correctly.
   ***************************************************************************/
  void currentTaskCallback(const std_msgs::String::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the status of the current mission.
   ***************************************************************************/
  void completedMissionCallback(const droneMsgsROS::CompletedMission::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes the action that has just been completed.
   ***************************************************************************/
  void completedActionCallback(const droneMsgsROS::CompletedAction::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Processes public events
   ***************************************************************************/
  void publicEventCallback(const droneMsgsROS::PublicEvent::ConstPtr& msg);

  /*!************************************************************************
   *  \brief   Detects if an external mission has been started.
   ***************************************************************************/
  void externalMissionStartedEvent(const droneMsgsROS::Event::ConstPtr& msg);

Q_SIGNALS:
  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the current action was received.
   *********************************************************************************************************************/
  void actionReceived(const QString action);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the curent task was received.
   *********************************************************************************************************************/
  void taskReceived(const QString task);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the mission name was received.
   *********************************************************************************************************************/
  void missionLoaded(const QString mission);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when errors in the mission specification file were detected.
   *********************************************************************************************************************/
  void missionErrors(const std::vector<std::string> error_messages);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the mission finished (successfully or unsuccessfully)
   *********************************************************************************************************************/
  void missionCompleted(const QString ack);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the mission starts.
   *********************************************************************************************************************/
  void missionStarted();

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the current action was completed (successfully or unsuccessfully).
   *********************************************************************************************************************/
  void actionCompleted(const int state, const int timeout);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent to indicate emergency land was issued.
   *********************************************************************************************************************/
  void emergencyLand();

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the current action's arguments were received.
   *********************************************************************************************************************/
  void actionArguments(const QString action_arguments);

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when some drone starts its mission.
   *********************************************************************************************************************/
  void commonMissionStarted();

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when all drones complete their missions.
   *********************************************************************************************************************/
  void commonMissionCompleted();

  /*!********************************************************************************************************************
   *  \brief      This signal is sent when the mission is started due to an external method.
   *********************************************************************************************************************/
  void externalMissionStarted();
};
#endif
