/*!************************************************************************************
 *  \file      obstacles_recevier.h
 *  \brief     Gets the obstacules (walls and poles) size and position from its
 *             corresponding topic (obstacules).
 *  \details   This file includes the headers of the class.
 *
 *  \author    Alberto Camporredondo
 *  \version   1.0
 *  \copyright   Copyright 2019 Universidad Politecnica de Madrid (UPM)
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#ifndef OBSTACLES_RECEIVER_H
#define OBSTACLES_RECEIVER_H

#include <ros/ros.h>
#include <QObject>
#include "droneMsgsROS/obstaclesTwoDim.h"
#include "droneMsgsROS/obstacleTwoDimWall.h"

class ObstaclesReceiver : public QObject
{
  Q_OBJECT

public:
  ObstaclesReceiver();
  ~ObstaclesReceiver();

  /*!************************************************************************
   *  \brief    This method opens the subscriptions that this process needs.
   ***************************************************************************/
  void openSubscriptions(ros::NodeHandle n, std::string ros_namespace);

  /*!************************************************************************
   *  \brief    This method checks wether the subscriptions have been opened already
   *  \return   Returns true if the subscriptions have been successfully opened, false otherwise.
   ***************************************************************************/
  bool isReady();

  /*!************************************************************************
   *  \brief   This method returns the map's walls vector.
   ***************************************************************************/
  droneMsgsROS::obstaclesTwoDim::_walls_type getWallsVector();

  /*!************************************************************************
   *  \brief   This method returns the map's poles vector.
   ***************************************************************************/
  droneMsgsROS::obstaclesTwoDim::_poles_type getPolesVector();

private:
  bool subscriptions_complete;
  std::string obstacle_topic;
  ros::Subscriber obstacle_sub;
  droneMsgsROS::obstaclesTwoDim obstacle_msg;
  droneMsgsROS::obstaclesTwoDim::_walls_type walls_vector;
  droneMsgsROS::obstaclesTwoDim::_poles_type poles_vector;

  /*!************************************************************************
   *  \brief    This method allows to refresh the drone's position within the map
   ***************************************************************************/
  void dronePositionCallback(const droneMsgsROS::obstaclesTwoDim::ConstPtr& msg);

Q_SIGNALS:
  void updateMapInfo();
};

#endif  // OBSTACLES_RECEIVER_H
